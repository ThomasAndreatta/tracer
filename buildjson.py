#!/usr/bin/python3

import time
from datetime import datetime
import requests
import json
from distutils.util import strtobool
from typing import NamedTuple
import argparse


input_file=''
out_file=''
malware_list_file=''
malware_list = {}
data={}
data['machine'] = []
append_to_previous=False
ip_list= {}
old_ip = []


def readMalware(malware_list_file):
    global malware_list
    with open(malware_list_file) as f:
        lines = f.readlines()
        lines = [line.rstrip() for line in lines]

    for line in lines:
        try:
            key=line.split(';')[0]
            if key not in malware_list:
                malware_list[key] = []
            malware_list[key].append(line.split(';')[1])
            if(len(line.split(';'))==3):
                malware_list[key].append(line.split(';')[2])
            else:
                malware_list[key].append('N/D')
        except Exception as e:
            print("Something went wrong while reading the malware list.\nCheck the format:\nmalwarename;hexColor(no #);malware description\nThe full error can be found in the 'error.log' file.")
            with open('error.log','w') as f:
                f.write(e)


def getip(file_name):
    global ip_list
    lines=[]

    with open(file_name) as f:
        lines = f.readlines()
        lines = [line.rstrip() for line in lines]

    try:
        i=0
        for vals in lines:
            key=vals.split(';')[0]
            if(key not in old_ip):
                i+=1
                if key not in ip_list:
                    ip_list[key] = []
                if(vals.split(';')[1].isnumeric()):
                    ip_list[key].append(vals.split(';')[1])
                else:
                    print(f"Something went wrong while reading the ip list.\nThe timestamp for the ip <{key}> at line <{i}> is not an int.")
                    exit(1)
                try:
                    ip_list[key].append(vals.split(';')[2])
                except Exception as e:
                    ip_list[key].append('N/D')

    except Exception as e:
        print("Something went wrong while reading the ip list.\nCheck the format:\nip;timestamp;(optional)malware type\nThe full error can be found in the 'error.log' file.")
        with open('error.log','w') as f:
            f.write(e)


def api(ip,timestamp,mal_type):
    global malware_list
    response = requests.get(f'http://ip-api.com/json/{ip}')
    el =response.json()

    if(el['status'] == 'fail'):
        print(f"\nJUMPED\nIP <{ip}> have recived a negative response from the tracing api.\n")
        return False

    if(mal_type not in malware_list):
        print(f"\nJUMPED\nIP <{ip}> the malware type has not been found in the malware list.\nappend_to_previous it to your malware list file or let it blank.\n")
        return False
    return {
    "timestamp": timestamp,
    "typemal": mal_type,
    "origin": el['city'] + ","+ el['country'],
    "fill": '#'+malware_list[mal_type][0],
    "ip": el['query'],
    "country":el['country'],
    "date":str(datetime.fromtimestamp(int(timestamp))),
    "lat":el['lat'],
    "long":el["lon"],
    "summary":malware_list[mal_type][1]
    }

def load_older(out_file):
    global append_to_previous,old_ip
    if(append_to_previous == True):
          with open(out_file) as jsonFile:
                data = json.load(jsonFile)
                for v in data['machine']:
                    old_ip.append(v['ip'])

def dump():
    if(append_to_previous == True):
        with open(out_file, "r+") as file:
            prv_data = json.load(file)
            prv_data.update(data)
            file.seek(0)
            json.dump(prv_data, file)
    else:
         with open(out_file, "w") as outfile:
            json.dump(data, outfile)

def main():
    global data,ip_list
    load_older(out_file)
    getip(input_file)
    readMalware(malware_list_file)
    l = len(ip_list)
    i=1
    for k in ip_list:
        if(i%10==0):
            time.sleep(8)
        val = api(k,ip_list[k][0],ip_list[k][1])
        if(val != False):
            data['machine'].append(val)
            print(f'{i}/{l}')
        i=i+1

    dump()



if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--input_file",default='clients', required=False, help="[STR] file with IP list")
    ap.add_argument("-o", "--output_file", default='info.json', required=False, help="[STR] FULL name of output file")
    ap.add_argument("-m", "--malware_file",default='malware.list', required=False, help="[STR] malware description file")
    ap.add_argument("-a", "--append",default='False',type=lambda x:bool(strtobool(x)), required=False, help="[BOOL] append to the output file if present")
    args = vars(ap.parse_args())
    input_file=args['input_file']
    out_file=args['output_file']
    malware_list_file=args['malware_file']
    append_to_previous=args['append']



    main()
