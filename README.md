<!-- PROJECT LOGO -->
<br />
<div align="center">
<img src="https://gitlab.com/ThomasAndreatta/tracer/-/raw/main/logo-mini.png" alt="Logo" width="120" height="120">
  </a>

  <h3 align="center">Malware Tracer</h3>

  <p align="center">
    An awesome README template to jumpstart your projects!
    <br />
    <br />
    <a href="https://www.redbitproject.org/example_live_view">View Demo</a>
    <a>|</a>
    <a href="https://gitlab.com/ThomasAndreatta/tracer/-/issues">Submit dataset</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
        <li><a href="#udage">Usage</a></li>
      </ul>
    </li>
    <li><a href="#different-applications">Different applications</a></li>
    <li><a href="#Malware-in-dataset">Malware in dataset</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
![](https://gitlab.com/ThomasAndreatta/tracer/-/raw/main/map.png)

Malware nowadays are always more frequent and tracing where they hit, origins and point is fundamental for understanding why and how.

I've decided to write a simple GUI based on a html and js which with the help of a .py script and a
base file of IPs,epoch-timestamp and malware family.


<p align="right">(<a href="#top">back to top</a>)</p>



### Usage

_Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services._

### Notes
- The *clients* file have to be formatted as follow\
  ip;epoch-timestamp;malware_name(OPTIONAL):
  ```txt
  184.105.247.243;1637763385;malware_name1
  202.1.171.207;975075385
  183.99.103.130;975075385;malware_name2
  ```
- The *malware* file have to be formatted as follows:
```txt
malware_name;hexColor(no #);(OPTIONAL)malware description
```
Always keep the "N/D" entry.
Adding a malware not present in the malware list will generate an error, if you don't have the right name to add in the clients list leave it empty.
- When adding a new malware to your list remember to add it even in the map.js series, template here:
```js
createSeries(
  'malwarename',
  crashesDataSet.filter('typemal', filterFunction('malwarename')),
  '#000000'
);
```
- The malware name have to be in lowercase.

---

1. Clone the repo
   ```sh
   git clone https://gitlab.com/ThomasAndreatta/tracer.git
   ```
2. Replace the "clients" file with your file, formatted as follow\
  ip;epoch-timestamp;malware_name(OPTIONAL):
  ```txt
  184.105.247.243;1637763385;malware_name1
  202.1.171.207;975075385
  183.99.103.130;975075385;malware_name2
  ```

  The malware name have to be present in your malware.list file or empty. The epoch timestamp have to be in Unix standard timestamp.

3. Run buildjson.py
   ```sh
   python3 ./buildjson.py --help
   ```
Could take a while, it uses a free API to retrive all the data connected to that IP, there's a sleep for avoiding anti-spam detection.
<p align="right">(<a href="#top">back to top</a>)</p>

4. Host it
	- Use some node server.
	- [Locally] Start a python3 webserver
		```sh
		sudo python3 -m http.server
		```
5. Enjoy
	- Open your browser and navigate to the nodejs server webpage.
	- [Locally] Navigate to the localhost page on port 80
		```
		http://localhost:80/index.html
		```



## Different applications

This projects can be used for showing basically anything on a map, with some slight modifications to the map.js code you can easily change the displayed data.
The only must have in the json file are the "lat" and "lon" field.

<p align="right">(<a href="#top">back to top</a>)</p>



## Malware in dataset

- [x] Mirai
- [x] personal_discovery

<p align="right">(<a href="#top">back to top</a>)</p>



## Contributing

If you have a dataset of infected machine IP with whichever type of virus - even one discovered by you - please let me know, would be great to add them on the map.
<p align="right">(<a href="#top">back to top</a>)</p>



## Contact

Twitter - [@T_Andreatta_](https://twitter.com/T_Andreatta_)\
Telegram - [@W_grizzly](https://t.me/W_grizzly)\
Project Link: [https://gitlab.com/ThomasAndreatta/tracer](https://gitlab.com/ThomasAndreatta/tracer)

<p align="right">(<a href="#top">back to top</a>)</p>

## Acknowledgments

Use this space to list resources you find helpful and would like to give credit to. I've included a few of my favorites to kick things off!

* [Client ip list](https://otx.alienvault.com/pulse/58adff48eddceb220bf3c654)
* [IP data API](https://ip-api.com/docs/api:json)
* [test json generator](https://www.json-generator.com/)


<p align="right">(<a href="#top">back to top</a>)</p>
