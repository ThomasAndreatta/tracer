anychart.onDocumentReady(function () {
// set chart theme
anychart.theme('darkBlue');
    // The data used in this sample can be obtained from the CDN
    // https://cdn.anychart.com/samples/maps-point-maps-dot-maps/airplane-crashes-since-1970-till-2009/data.json
    anychart.data.loadJsonFile(
      './info.json',
      function (data) {
        data = data['machine'];
        console.log(data);
        // sets map chart
        var map = anychart.map();
        map.geoData('anychart.maps.world').padding(0);

        map
          .unboundRegions()
          .enabled(true)
          .fill('#E1E1E1')
          .stroke('#D2D2D2');

        // sets credits for this sample
        map
          .credits()
          .enabled(true);

        // sets chart title
        map
          .title()
          .enabled(true)
          .useHtml(true)
          .padding([20, 0, 10, 0])
          .text(
            'Compromised endpoints<br/>' +
            '<span style="color:#929292; font-size: 12px;">' +
            '</span>'
          );
        // creates Dataset from Sample data
        var dataset =data.sort(GetSortOrder("timestamp"));
        var crashesDataSet = anychart.data.set(data).mapAs();


        //recent attacks
        var n;
        if (dataset.length > 5)
          n = 5;
        else
          n = dataset.length;


        var labeltxt='<span style="font-size: 13px"><span style="color: #ff0000">Latest attacks:</span><br/><br/>';

        for (var i = 0; i < n; i++) {
          labeltxt+=  '<span style="font-size: 13px"><span style="color: #bfbfbf">'+dataset[i].country+'| </span>' +
            dataset[i].ip+'<br/><br/>';
        }


        map
          .label('2')
          .useHtml(true)
          .padding([1, 2,2, 40])
          .position("leftbottom")
          .anchor("leftbottom")
          .width(350)
          .fontSize(10)
          .fontColor('#e6e6e6')
          .text(labeltxt)
          //end recent attacks


        //most attacked

      var labeltxt2='<span style="font-size: 13px"><span style="color: #ff0000">Most targetted:</span><br/><br/>';
        var list2 = dataset.reduce(function(sums,entry){
           sums[entry.country] = (sums[entry.country] || 0) + 1;
           return sums;
        },{});
        var items = Object.keys(list2).map(function(key) {
        return [key, list2[key]];
      });

      // Sort the array based on the second element
      items.sort(function(first, second) {
        return second[1] - first[1];
      });

      // Create a new array with only the first 5 items
      var ll = Object.keys(data).length;
      if(Object.keys(data).length >= 3)
        ll = 3;

      list2 = items.slice(0, ll);
        for (var i = 0; i <ll; i++) {
          labeltxt2 +=  '<span style="font-size: 13px"><span style="color: #bfbfbf">'+list2[i][0]+'| </span>' +
            list2[i][1]+'<br/><br/>';
        }


        map
          .label('1')
          .useHtml(true)
          .padding([15, 20,20, 40])
          .position("lefttop")
          .anchor("lefttop")
          .width(350)
          .fontSize(10)
          .fontColor('#e6e6e6')
          .text(labeltxt2)





        // helper function to create several series
        var createSeries = function (name, data, color) {
          // sets marker series and series settings
          var series = map.marker(data);
          series
            .name(name)
            .fill(color)
            .stroke('2 #E1E1E1')
            .type('circle')
            .size(8)
            .labels(false)
            .selectionMode('none');

          series.hovered().stroke('2 #fff').size(8);

          series
            .legendItem()
            .iconType('circle')
            .iconFill(color)
            .iconStroke('2 #E1E1E1');
        };

        // creates series
        createSeries(
          'N/D',
          crashesDataSet.filter('typemal', filterFunction('N/D')),
          '#cf03fc'
        );
        createSeries(
          'c2c',
          crashesDataSet.filter('typemal', filterFunction('c2c')),
          '#fcba03'
        );
        createSeries(
          'mirai',
          crashesDataSet.filter('typemal', filterFunction('mirai')),
          '#900713'
        );
        //ADD SERIES HERE


        // Enables map tooltip and sets settings for tooltip
        map.tooltip().title().fontColor('#fff');
        map.tooltip().titleFormat(function () {
          return this.getData('date');
        });

        map
          .tooltip()
          .useHtml(true)
          .padding([8, 13, 10, 13])
          .width(350)
          .fontSize(12)
          .fontColor('#e6e6e6')
          .format(function () {
            var summary = '<br/><br/>' + this.getData('summary');
            if (this.getData('summary') === 'null') summary = '';
            return (
              '<span style="font-size: 13px"><span style="color: #bfbfbf">Type: </span>' +
              this.getData('typemal') +
              '<br/>' +
              '<span style="color: #bfbfbf">Place: </span>' +
              this.getData('origin') +
              '<br/>' +
              '<span style="color: #bfbfbf">IP: </span>' +
              this.getData('ip') +
              '<br/>' +

              summary
            );
          });

        // turns on the legend for the sample
        map.legend(true);

        // create zoom controls
        var zoomController = anychart.ui.zoom();
        zoomController.render(map);

        // sets container id for the chart
        map.container('container');

        // initiates chart drawing
        map.draw();
      }
    );
  });


  function GetSortOrder(prop) {
      return function(a, b) {
          if (a[prop] < b[prop]) {
              return 1;
          } else if (a[prop] > b[prop]) {
              return -1;
          }
          return 0;
      }
  }


  // helper function to bind data field to the local var.
  function filterFunction(val1) {

    if (val1) {

        return function (fieldVal) {
          return val1 === fieldVal;
        };
      }


    }
